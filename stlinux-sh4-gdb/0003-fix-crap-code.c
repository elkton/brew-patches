diff --git a/gdb/stlinux/lkd-modules.c b/gdb/stlinux/lkd-modules.c
index 085701a..8d2ae88 100644
--- a/gdb/stlinux/lkd-modules.c
+++ b/gdb/stlinux/lkd-modules.c
@@ -801,6 +801,15 @@ set_section_contents (bfd * abfd, asection * sectp, void *i)
   do_cleanups (clean);
 }
 
+static int cmp_sections (void *base, const void *s1, const void *s2)
+{
+  asection **sec1 = (asection **) s1;
+  asection **sec2 = (asection **) s2;
+  bfd *abfd = (bfd*)base;
+  return bfd_get_section_vma (abfd, *sec1)
+    - bfd_get_section_vma (abfd, *sec2);
+}
+
 /* This is the function that will create a 'linked' (ie. fully
  relocated) module file from the real module file that got
  loaded. ABFD points to the bfd for the read file, LM_INFO gives the
@@ -896,14 +905,6 @@ make_temporary_bfd (bfd * abfd, struct lm_info *lm_info, char **temp_pathname)
     asection *hdrpp;
     bfd_size_type amt;
 
-    int cmp_sections (const void *s1, const void *s2)
-    {
-      asection **sec1 = (asection **) s1;
-      asection **sec2 = (asection **) s2;
-      return bfd_get_section_vma (abfd, *sec1)
-	- bfd_get_section_vma (abfd, *sec2);
-    }
-
     amt = sizeof (struct elf_segment_map);
     amt += (bfd_count_sections (info.new)) * sizeof (asection *);
     m = bfd_zalloc (info.new, amt);
@@ -916,7 +917,8 @@ make_temporary_bfd (bfd * abfd, struct lm_info *lm_info, char **temp_pathname)
 	m->sections[j++] = hdrpp;
     m->count = j;
 
-    qsort (m->sections, m->count = j, sizeof (m->sections[0]), cmp_sections);
+    qsort_r (m->sections, m->count = j, sizeof (m->sections[0]),
+	     abfd, cmp_sections);
 
     elf_seg_map (info.new) = m;
   }
@@ -1139,30 +1141,30 @@ try_to_open_alternate_names (char *file, char **temp_pathname)
   int level = 0;
   char *filename;
 
-  void add_char (int pos)
-  {
-
-    if (nb_chars == allocated_chars)
-      {
-	allocated_chars *= 2;
-	tree = xrealloc (tree, sizeof (int) * allocated_chars);
-      }
-
-    tree[nb_chars] = pos;
-    filename[pos] = '\0';
-    ++nb_chars;
+  #define add_char(pos) \
+  { \
+\
+    if (nb_chars == allocated_chars) \
+      { \
+	allocated_chars *= 2; \
+	tree = xrealloc (tree, sizeof (int) * allocated_chars); \
+      } \
+\
+    tree[nb_chars] = pos; \
+    filename[pos] = '\0'; \
+    ++nb_chars; \
   }
 
-  void gather_chars ()
-  {
-    char *c = filename;
-
-    while (*c)
-      {
-	if (*c == '_')
-	  add_char (c - filename);
-	++c;
-      }
+  #define gather_chars() \
+  { \
+    char *c = filename; \
+\
+    while (*c)\
+      {\
+	if (*c == '_')\
+	  add_char (c - filename);\
+	++c;\
+      }\
   }
 
   filename = alloca (strlen (file) + 1);
diff --git a/gdb/stlinux/lkd.h b/gdb/stlinux/lkd.h
index 5457be7..246f7a3 100644
--- a/gdb/stlinux/lkd.h
+++ b/gdb/stlinux/lkd.h
@@ -619,7 +619,7 @@ void lkd_uninstall_do_exit_event (void);
 
 char *read_dentry (CORE_ADDR dentry);
 
-inline void page_error_clear (void);
+void page_error_clear (void);
 
 void sanitize_path (char *path);
 char *linux_aware_get_target_root_prefix (void);
